---
name: National Archives Citizen Archivist
website: https://www.archives.gov/citizen-archivist/volunteers
categories: History, Transcription, Photo
license: Public Domain
organization_type: Government
---

The Citizen Archivist program from the National Archives gives everyday citizens the opportunity to contribute to the collective knowledge of the United States through photo tagging and transcription projects.
