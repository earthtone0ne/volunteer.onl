---
name: Learning Ally
website: https://learningally.org
volunteer_page: https://go.learningally.org/Get-Involved/Volunteer-Opportunities
categories: Literature, Reading, Proofreading
license: Proprietary
organization_type: 501c3
---

Learning Ally helps students with reading disabilities in a number of ways, including emotional and educational support. Volunteer opportunities exist for a number of rolls, including narration and proofreader.
