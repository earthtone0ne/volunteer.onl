---
name: Be My Eyes
website: https://www.bemyeyes.com/
categories: Literature, Proofreading
organization_type: Company (Corporate Volunteering)
---

Be My Eyes connects visually impaired individuals with volunteers through their app. Volunteers help by solving a variety of visual tasks, including identifying objects, reading,and more.

