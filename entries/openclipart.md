---
name: OpenClipArt
website: https://openclipart.org
categories: Art
volunteer_page: https://openclipart.org/join
license: CC0
---

OpenClipArt is a collection of vector graphics that you can use without restriction in your projects with absolutely no restrictions. You can download and use any of the clip art, or contribute your own works to the project!
