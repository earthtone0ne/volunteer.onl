---
name: Wikivoyage
organization_type: 501c3
categories: Wikimedia
license: CC BY-SA 3.0
website: https://wikivoyage.org
---

Wikivoyage is a free online travel guide created and currated by thousands of worldwide volunteers. As an Wikimedia project, it runs the same software and has the same interface as Wikipedia, making it easy to start contributing.

