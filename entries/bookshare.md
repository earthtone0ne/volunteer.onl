---
name: Bookshare
website: https://bookshare.org
volunteer_page: https://www.bookshare.org/cms/get-involved/volunteer
categories: Literature, Proofreading
organization_type: 501c3
---

Bookshare helps make reading more accessible for those with reading disabilities. It does this through making specialized versions of the book material, or making them available as audiobooks for those who need it. You can volunteer as a person to scan books, to proofread them, or by helping organize others into getting involved.
