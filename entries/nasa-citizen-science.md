---
name: NASA's Citizen Science
website: https://science.nasa.gov/citizenscience
license: Public Domain
organization_type: Government
categories: Science
---

NASA's Citizen Science portal connects science-minded citizens with NASA supported projects around the world where they can contribute and collaborate in helping learn more about the universe, understand climate change, monitor asteroids and exoplanets, and more.
