---
name: Distributed Proofreaders
website: https://pgdp.net
volunteer_page: https://www.pgdp.net/c/
categories: Literature, Proofreading
license: Public Domain
---

Distributed Proofreaders works to make books available on [Project Gutenberg](https://www.gutenberg.org/), the largest collection of Public Domain works on the Internet. Volunteers proofread already scanned and OCRed texts for errors together until a book is ready tobe placed in the collection.
