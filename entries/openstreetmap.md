---
name: OpenStreetMap
website: https://openstreetmap.org
license: ODbL
categories: Mapping, OpenStreetMap
pinned: OpenStreetMap
---

OpenStreetMap is a free map that anyone can contribute to. There are many ways to contribute to the project, includng editing the map through the online editor, collecting information using local knowledge, tracing imagery and more. Many geographical projects use OpenStreetMap as their foundation.
