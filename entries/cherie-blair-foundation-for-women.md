---
name: Clerie Blair Foundation For Women
website: https://cherieblairfoundation.org
volunteer_page: https://cherieblairfoundation.org/programmes/mentoring/
categories: Entrepreneurship, Mentoring, Women
organization_type: UK Charity
---

The Claire Blair Foundation for Women matches enterpreneurs from low and middle income countries with mentors (both women and men) from all over the world. They work together for a period of a year on the the mentee's business and personal goals, which in turn helps not only the mentee, but their family and community as well.

