---
name: Freesound
website: https://freesound.org/
categories: Audio
license: Misc (all Freely Licensed)
---

Freesound is a collection of freely licensed sound effects that you can use in your work. This collection includes nature sounds, everyday sound, cartoony sounds, and everything in between. You can record and submit your own audio clips for inclusion in this database for people to use in their audio or video projects.

