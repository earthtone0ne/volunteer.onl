---
name: Nepris
website: https://nepris.com
volunteer_page: https://www.nepris.com/volunteer/opportunities
license: Proprietary
organization_type: Company
categories: Teaching, Video
---

Nepris is a classroom learning tool that connects educators with experts in a field. These experts schedule a session with an educator to talk about their passion and subject matter to the classroom. Educators may also browse videos of precious sessions. Volunteer experts can sign up to present their material.
