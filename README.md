GoodWorksOnline
================

This repository contains the source code for the GoodWorksOnline.org website.

You should generally not need to self-host this project, but you are free to do so as per the [license](LICENSE.md), so long as you properly attribute this project.

Contributing
-------------

If you would like to contribute to the project, either by adding a project entry, correcting an error, or improve the site in some way, please read the [Contribution Guide](CONTRIBUTING.md).

Installation
-------------

You most likely are not needing to self-host this project, but if you really do want to, this section will explain how.

If you don't have Python Poetry installed, you must install it:

`pip install poetry`

Then install the project dependencies:

`poetry install`

Then finally you can run the build process:

`poetry run build`

This will result in a `public` directory at the root of the repository, which contains the website.

