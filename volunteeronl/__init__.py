__version__ = '0.1.0'

from flask import Flask
from .app import app
from .freeze import freeze
